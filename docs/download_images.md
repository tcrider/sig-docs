# Downloading images produced by the Automotive SIG

The Automotive SIG provides OSBuild manifests and instructions to build AArch64 or x86_64 images
for systems, virtual machines, or a Raspberry Pi 4. If you would like to view our images, follow
the instructions on the [Building Images](https://sigs.centos.org/automotive/building/) page.

In an effort to continuously deliver working images, we are building nightly a number images
of images which can be found in our nightly folder at:
[https://autosd.sig.centos.org/AutoSD-9/nightly/](https://autosd.sig.centos.org/AutoSD-9/nightly/)

These images are updated as CentOS-stream, AutoSD, or the Automotive SIG RPM repository changes.

Here are some information about the different images built:

In the nightly folder you can find three sub-folders:

- `repos`: This folder contains the nightly build of the AutoSD RPM repository for both
  x86_64 and aarch64.
- `sample-images`: This folder contains a set of images built with only the content
  of the AutoSD RPM repository.
- `non-sample-images`: This folder contains a set of images built from the AutoSD RPM
  repository as well as other RPM repositories (e.g.: the CentOS-stream 9 repository,
  the Automotive SIG RPM repository, [COPR](https://copr.fedorainfracloud.org/) repositories...)

## Sample images

You can find 5 images in the `sample-images` folder:

- Ostree-based minimal qemu images built for x86_64 and aarch64, this image is really meant to
  be an example of a minimal image, it does not have a package manager nor SSH.
  These images are:
    - `auto-osbuild-qemu-autosd9-minimal-ostree-aarch64`
    - `auto-osbuild-qemu-autosd9-minimal-ostree-x86_64`

- Ostree-based qemu images built for our own QA/testing, for x86_64 and aarch64. It is based on
  the minimal image with some extra packages such as: SSH, beaker, rsync, sudo, wget, time, nfs-utils,
  git, jq.
  These images are:
    - `auto-osbuild-qemu-cs9-qa-ostree-aarch64`
    - `auto-osbuild-qemu-cs9-qa-ostree-x86_64`

- A regular (i.e.: not ostree-based) minimal image, built for raspberry pi 4.
  This image is:
    - `auto-osbuild-rpi4-cs9-minimal-regular-aarch64`

## Non Sample images

You can find 10 images in the `non-sample-images` folder:

- Ostree-based qemu images built for x86_64 and aarch64 demonstrating how to include containers
  in images built.
  These images are:
    - `auto-osbuild-qemu-cs9-container-ostree-aarch64`
    - `auto-osbuild-qemu-cs9-container-ostree-x86_64`

- Ostree-based qemu images built for developers which includes all the tooling necessary
  to build images as described in the [Building images](building/index.md) page.
  These images are:
    - `auto-osbuild-qemu-cs9-developer-ostree-aarch64`
    - `auto-osbuild-qemu-cs9-developer-ostree-x86_64`

- A regular (i.e.: not ostree-based) qemu image built for developers which thus includes all
  the tooling necessary
  This image is:
    - `auto-osbuild-qemu-cs9-developer-regular-aarch64`

- Ostree-based qemu images built with the neptune demonstration application
  These images are:
    - `auto-osbuild-qemu-cs9-neptune-ostree-aarch64`
    - `auto-osbuild-qemu-cs9-neptune-ostree-x86_64`

- A regular (i.e.: not ostree-based) raspberry pi 4 image built for developers which thus
  includes all the tooling necessary
  This image is:
    - `auto-osbuild-rpi4-cs9-developer-regular-aarch64`

- A regular (i.e.: not ostree-based) raspberry pi 4 image built to be used as
  [USB gadget](building/gadget.md)
  This image is:
    - `auto-osbuild-rpi4-cs9-gadget-regular-aarch64`

- A regular (i.e.: not ostree-based) raspberry pi 4 image built with the neptune
  demonstration application:
  This image is:
    - `auto-osbuild-rpi4-cs9-neptune-regular-aarch64`
